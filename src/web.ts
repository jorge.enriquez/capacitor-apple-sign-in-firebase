import { WebPlugin } from "@capacitor/core";
import { AppleSigInFirebasePlugin } from "./definitions";

export class AppleSigInFirebaseWeb extends WebPlugin
  implements AppleSigInFirebasePlugin {
  constructor() {
    super({
      name: "AppleSigInFirebase",
      platforms: ["web"],
    });
  }

  async signIn(options: { idToken: string }): Promise<{ value: string }> {
    console.log("ECHO", options);
    return { value: "" };
  }
}

const AppleSigInFirebase = new AppleSigInFirebaseWeb();

export { AppleSigInFirebase };

import { registerWebPlugin } from "@capacitor/core";
registerWebPlugin(AppleSigInFirebase);
