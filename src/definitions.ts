declare module "@capacitor/core" {
  interface PluginRegistry {
    AppleSigInFirebase: AppleSigInFirebasePlugin;
  }
}

export interface AppleSigInFirebasePlugin {
  signIn(options: { idToken: string }): Promise<{ value: string }>;
}
