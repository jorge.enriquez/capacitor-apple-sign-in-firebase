
  Pod::Spec.new do |s|
    s.name = 'CapacitorAppleSignInFirebase'
    s.version = '0.0.1'
    s.summary = 'Send Apple Sin in token to native firebase'
    s.license = 'MIT'
    s.homepage = 'https://gitlab.com/jorge.enriquez/capacitor-apple-sign-in-firebase'
    s.author = 'Jorge Enriquez'
    s.source = { :git => 'https://gitlab.com/jorge.enriquez/capacitor-apple-sign-in-firebase', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '11.0'
    s.dependency 'Capacitor'
  end