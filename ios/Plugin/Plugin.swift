import Foundation
import Capacitor
import Firebase

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitor.ionicframework.com/docs/plugins/ios
 */
@objc(AppleSigInFirebase)
public class AppleSigInFirebase: CAPPlugin {

    @objc func signIn(_ call: CAPPluginCall) {
        let idToken = call.getString("idToken") ?? ""
        let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: idToken, accessToken: nil)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            guard error != nil else {
                call.reject("Error", error)
                return
            }

          call.success([
              "user": authResult
          ])
        }

    }
}
